package com.arrkdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arrkdemo.adapters.StarWarsCharactersListAdapter;
import com.arrkdemo.models.StarWarsCharacters;
import com.arrkdemo.utils.Constants;
import com.arrkdemo.utils.HTTPConnectionManager;
import com.arrkdemo.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class StarWarsCharactersList extends AppCompatActivity {

    Context mContext;
    RecyclerView mStarWarsCharactersList;
    ProgressDialog mProgressDialog;
    String mServerResponse;
    ArrayList<StarWarsCharacters> mStarWarsModelList;
    StarWarsCharactersListAdapter mAdapter;
    Button mTryAgainButton;
    TextView mNoDataAvailableText;
    static final String BUNDLE_VALUE_TAG = "Character_Objects";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star_wars_charachters_list);

        getSupportActionBar().setTitle(getString(R.string.characters_screen));
        mContext = this;
        mStarWarsCharactersList = (RecyclerView) findViewById(R.id.star_wars_characters_list);
        mTryAgainButton = (Button) findViewById(R.id.btn_try_again);
        mNoDataAvailableText = (TextView) findViewById(R.id.noDataAvailableLabel);

        if (Utils.isNetworkAvailable(mContext)) {
            CharacterListDisplayTask characterListDisplayTask = new CharacterListDisplayTask();
            characterListDisplayTask.execute();
        }
        else{
            mNoDataAvailableText.setVisibility(View.VISIBLE);
            mStarWarsCharactersList.setVisibility(View.GONE);
        }

        mTryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(mContext)) {
                    CharacterListDisplayTask characterListDisplayTask = new CharacterListDisplayTask();
                    characterListDisplayTask.execute();
                }
            }
        });
    }

    private class CharacterListDisplayTask extends AsyncTask<String, String, String>{
        @Override
        protected String doInBackground(String... params) {

            try {
                HTTPConnectionManager connectionManager = new HTTPConnectionManager(getString(R.string.starships));
                mServerResponse = connectionManager.connectNow();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        public void onPreExecute() {

            mProgressDialog = ProgressDialog.show(StarWarsCharactersList.this, "",
                    getString(R.string.please_wait), true);
            mProgressDialog.setCancelable(false);

        }

        @Override
        protected void onPostExecute(String s) {
            mProgressDialog.dismiss();

            if(!TextUtils.isEmpty(mServerResponse)){
                JSONObject jsonObject = null;
                JSONArray results;
                try {
                    jsonObject = new JSONObject(mServerResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jsonObject!=null) {

                    mNoDataAvailableText.setVisibility(View.GONE);
                    mStarWarsCharactersList.setVisibility(View.VISIBLE);
                    mStarWarsModelList = new ArrayList<>();
                    try {
                        results = jsonObject.getJSONArray("results");
                        for(int i=0;i<results.length();i++){

                            StarWarsCharacters starWarsCharacters = new StarWarsCharacters();
                            JSONObject items = results.getJSONObject(i);
                            String name = items.getString("name");
                            String height = items.getString("length");
                            String mass = items.getString("cargo_capacity");
                            String dateTime = items.getString("created");

                            starWarsCharacters.setName(name);
                            starWarsCharacters.setDateTime(dateTime);
                            starWarsCharacters.setHeight(height);
                            starWarsCharacters.setMass(mass);

                            mStarWarsModelList.add(starWarsCharacters);
                        }

                        mAdapter = new StarWarsCharactersListAdapter(mContext, mStarWarsModelList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        mStarWarsCharactersList.setLayoutManager(mLayoutManager);
                        mStarWarsCharactersList.setItemAnimator(new DefaultItemAnimator());
                        mStarWarsCharactersList.setAdapter(mAdapter);

                        mStarWarsCharactersList.addOnItemTouchListener(new RecyclerTouchListener(mContext, mStarWarsCharactersList, new RecyclerTouchListener.ClickListener() {
                            @Override
                            public void onClick(View view, int position) {
                                Intent intent = new Intent(mContext, CharacterDetailView.class);
                                intent.putExtra(BUNDLE_VALUE_TAG, mStarWarsModelList.get(position));
                                startActivity(intent);
                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));

                } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    Toast.makeText(mContext,getString(R.string.try_again),Toast.LENGTH_SHORT).show();
                    mNoDataAvailableText.setVisibility(View.VISIBLE);
                    mStarWarsCharactersList.setVisibility(View.GONE);
                }
            }
        }
    }
}
