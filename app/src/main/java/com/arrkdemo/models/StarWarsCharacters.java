package com.arrkdemo.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by intel on 5/24/2018.
 */

public class StarWarsCharacters implements Parcelable {

    private String name;
    private String height;
    private String mass;
    private String dateTime;


    public StarWarsCharacters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.height);
        dest.writeString(this.mass);
        dest.writeString(this.dateTime);
    }

    protected StarWarsCharacters(Parcel in) {
        this.name = in.readString();
        this.height = in.readString();
        this.mass = in.readString();
        this.dateTime = in.readString();
    }

    public static final Creator<StarWarsCharacters> CREATOR = new Creator<StarWarsCharacters>() {
        @Override
        public StarWarsCharacters createFromParcel(Parcel source) {
            return new StarWarsCharacters(source);
        }

        @Override
        public StarWarsCharacters[] newArray(int size) {
            return new StarWarsCharacters[size];
        }
    };
}
