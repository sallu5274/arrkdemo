package com.arrkdemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.arrkdemo.R;
import com.arrkdemo.models.StarWarsCharacters;

import java.util.ArrayList;

/**
 * Created by intel on 5/24/2018.
 */

public class StarWarsCharactersListAdapter extends RecyclerView.Adapter<StarWarsCharactersListAdapter.MyViewHolder> {

    private ArrayList<StarWarsCharacters> charactersArrayList;
    private Context mContext;


    public StarWarsCharactersListAdapter(Context context, ArrayList<StarWarsCharacters> merchants) {
        mContext = context;
        charactersArrayList = merchants;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int position = 0;
        private  TextView name;

        public void setPosition(int position) {
            this.position = position;
        }

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) itemView.findViewById(R.id.names);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, "Position " + position, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.star_wars_list_items, parent, false);

        return new StarWarsCharactersListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StarWarsCharactersListAdapter.MyViewHolder holder, int position) {

        holder.name.setText(charactersArrayList.get(position).getName());
        holder.setPosition(position);
    }

    @Override
    public int getItemCount() {
        return charactersArrayList.size();
    }
}
