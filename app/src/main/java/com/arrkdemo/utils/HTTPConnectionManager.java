package com.arrkdemo.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by intel on 5/25/2018.
 */

public class HTTPConnectionManager {

    URL url;
    HttpURLConnection urlConnection = null;
    private String server_response;

    public HTTPConnectionManager(String suffix){

        try {
            url = new URL(Constants.BASE_URL + suffix);
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String connectNow() {

        int responseCode = 0;
        try {
            responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                server_response = readStream(urlConnection.getInputStream());
                Log.d("ARRK", "RESPONSE " + server_response);
                return server_response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }
}
