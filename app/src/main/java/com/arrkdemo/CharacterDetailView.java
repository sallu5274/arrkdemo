package com.arrkdemo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.arrkdemo.models.StarWarsCharacters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class CharacterDetailView extends AppCompatActivity {

    Context mContext;
    TextView mNameValue;
    TextView mHeightValue;
    TextView mMassValue;
    TextView mCreatedValue;
    StarWarsCharacters starWarsCharacterData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_detail_view);

        overridePendingTransition(R.anim.activity_transition_slide_in_new,
                R.anim.activity_transition_fade_out_prev);

        getSupportActionBar().setTitle(getString(R.string.detail_screen));
        mContext = this;
        mNameValue = (TextView) findViewById(R.id.name_value);
        mHeightValue = (TextView) findViewById(R.id.height_value);
        mMassValue = (TextView) findViewById(R.id.mass_value);
        mCreatedValue = (TextView) findViewById(R.id.created_value);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            starWarsCharacterData = bundle.getParcelable(StarWarsCharactersList.BUNDLE_VALUE_TAG);
        }

        if(starWarsCharacterData!=null){
            mNameValue.setText(starWarsCharacterData.getName());
            mHeightValue.setText(starWarsCharacterData.getHeight()+" meters");
            mMassValue.setText(starWarsCharacterData.getMass()+ " Kg");
            mCreatedValue.setText(formatDateTime(starWarsCharacterData.getDateTime()));
        }
    }

    private String formatDateTime(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat formateDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try{
            Date date = sdf.parse(dateTime);
            return formateDate.format(date);
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }

    }
}
